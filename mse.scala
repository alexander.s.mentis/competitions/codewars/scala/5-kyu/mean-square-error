object Sol {

  def solution(a: Array[Int], b: Array[Int]): Double = {
    val sqrDiff = a.zip(b).map{case (a, b) => (a-b)*(a-b)} 
    sqrDiff.sum.toDouble / sqrDiff.length
  }
}